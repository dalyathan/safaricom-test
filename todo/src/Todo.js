import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Modal from 'react-bootstrap/Modal';
import {backEndURL} from './constants';
import dayjs from 'dayjs';
import {useState} from 'react';
function Todo(props){
    const humanizeDuration = require("humanize-duration");
    let [showModal, setShowModal]= useState(false);
    let [name, setName]= useState(props.name);
    let [time, setTime]= useState(props.time);
    let [due, setDue]= useState('');
    function updateDue(){
        const now= new Date();
        setDue(humanizeDuration(dayjs(time).diff(dayjs(now))));
    } 
    setTimeout(updateDue,1000);
    function updateTodo(e) {
        e.preventDefault();
        fetch(`${backEndURL}/${props.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(
                {
                    "name": name,
                    "time": time
                }
            ),
        })
            
    }
    function deleteTodo(e) {
        e.preventDefault();
        fetch(`${backEndURL}/${props.id}`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            
    }
    return (
        <>
        <Modal show={showModal} onHide={()=>setShowModal(false)}>
            <Modal.Header closeButton>
            <Modal.Title>Edit Todo</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <input onChange={(event)=>setName(event.target.value)} value={name}/>
                <input onChange={(event)=>setTime(event.target.value)} type="date" value={time}/>
            </Modal.Body>
            <Modal.Footer>
            <Button variant="primary" onClick={updateTodo}>
                Save Changes
            </Button>
            <Button variant="primary" onClick={deleteTodo}>
                Delete
            </Button>
            </Modal.Footer>
        </Modal>
        <Card style={{ width: '18rem' }} onClick={()=> setShowModal(true)}>
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">Due in {due}</Card.Subtitle>
          </Card.Body>
        </Card>
        </>
      );
}

export default Todo;