import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Modal from 'react-bootstrap/Modal';
import dayjs from 'dayjs';
import {useState} from 'react';
function EditModal({show, name, time,id}){
    let [showModal, setShowModal]= useState(show);
    return  <Modal show={showModal} onHide={()=>setShowModal(false)}>
                <Modal.Header closeButton>
                <Modal.Title>Modal heading</Modal.Title>
                </Modal.Header>
                <Modal.Body>Woohoo, you are reading this text in a modal!</Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={()=>setShowModal(false)}>
                    Close
                </Button>
                <Button variant="primary" onClick={()=>setShowModal(false)}>
                    Save Changes
                </Button>
                </Modal.Footer>
            </Modal>
}
export default EditModal;