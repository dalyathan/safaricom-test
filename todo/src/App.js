import React from 'react';
import logo from './logo.svg';
import Todos from './Todos';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Todos</h1>
       <Todos/>
      </header>
    </div>
  );
}

export default App;
