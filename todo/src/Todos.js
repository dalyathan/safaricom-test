import Todo from './Todo';
import {backEndURL} from './constants';
import {useEffect, useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
function Todos(){
    // const todos=[1,2,3];
    let [todos, setTodos]= useState([]);
    let [showNewTodoModal, setShowNewTodoModal]=useState(false);
    let [name, setName]= useState('');
    let [time, setTime]= useState('');
    function createTodo(e) {
        e.preventDefault();
        console.log(name,time);
        fetch(backEndURL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(
                {
                    "name": name,
                    "time": time
                }
            ),
        }).then((response) => {
            return response.json();
        })
        .then((data) => {
            setTodos([...todos,data.todo])
        })
            
    }
    useEffect(() => {
        fetch(backEndURL, {
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                setTodos(data.todos);
               
            })
    }, []);
    return <>
        {todos.map((i)=> <Todo name={i.name} time={i.time} id={i._id} key={i._id}/>)}
        <Modal show={showNewTodoModal} onHide={()=>setShowNewTodoModal(false)}>
            <Modal.Header closeButton>
            <Modal.Title>Create Todo</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <input onChange={(event)=>setName(event.target.value)} value={name}/>
                <input onChange={(event)=>setTime(event.target.value)} type="date" value={time}/>
            </Modal.Body>
            <Modal.Footer>
            <Button variant="primary" onClick={createTodo}>
                Save Changes
            </Button>
            </Modal.Footer>
        </Modal>
        <Button variant="primary" onClick={()=>setShowNewTodoModal(true)}>
            New Todo
        </Button>
    </>

}

export default Todos;