const express= require('express');
const cors= require('cors')
const app= express();
const mongoose= require('mongoose');
const Todo = require('./models/todo'); 
mongoose.set('strictQuery', false);

require('dotenv').config();


app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended: true}));

const port=process.env.PORT || 3000;
const connectionString= process.env.CONNECTION;

app.get('/', async (req, res)=>{
   const result= await Todo.find();

    res.send(result)
})


app.get('/api/todos',async ( req, res)=>{
    try{
        const todos= await Todo.find();
        res.json({"todos": todos});
    }catch(e){
        res.send(500).json({error: e.message})
    }
})

app.get('/api/todos/:id', async (req, res)=>{
    const {id: todoId}= req.params;
    try{
        const todo= await Todo.findById(todoId);
        res.status(200).json(todo);
    }catch(e){
        res.status(500).json({error: e.message});
    }
})

app.put('/api/todos/:id', async (req, res)=>{
    const {id: todoId}= req.params;
    try{
        const result= await Todo.findOneAndReplace({_id: todoId}, req.body, {new: true});
        res.status(200).json(result);
    }catch(e){
        res.status(500).json({error: e.message});
    }
})

app.delete('/api/todos/:id', async (req, res)=>{
    try{
        const result= await Todo.deleteOne({_id: req.params.id});
        res.json({deletedCount: result.deletedCount})
    }catch(e){
        res.status(500).json({error: e.message});
    }
})

app.post('/api/todos', async (req, res)=>{
    console.log(req.body);
    const todo= new Todo(req.body);
    try{
        await todo.save();
        res.status(201).json({todo});
    }catch(e){
        res.status(400).json({error: e.message})
    }
})

app.post('/',(req, res)=>{
    res.send('This  menkir is a post request');
})


const start= async ()=>{
    try{
        await mongoose.connect(connectionString);
        app.listen(port, ()=>{
            console.log('listening on port', port);
        })
    }catch(e){
        console.log(e.message);
    }
}
 start();