const mongoose= require('mongoose');

const todoSchema= new mongoose.Schema({
    name: String,
    time: String,
})

module.exports= mongoose.model('Todo', todoSchema);